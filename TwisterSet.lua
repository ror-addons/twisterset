local MAX_SETS					= 5;


TwisterSet = {};
TwisterSet.currentSet= 0;



-- This function initialize the TwisterSet Window and ComboBox
function TwisterSet.Initialize()
    if GameData.Player.career.line == GameData.CareerLine.KNIGHT or
       GameData.Player.career.line == GameData.CareerLine.CHOSEN then
       
        -- Creates the window and shows it
        CreateWindow("TwisterSetWnd", true)
    
        -- Register the window with the Layout Editor
        LayoutEditor.RegisterWindow( "TwisterSetWnd",
                                L"TwisterSet",
                                L"Quick Switch of Twister Set",
                                false, false,
                                true, nil )

        --TwisterSetComboBox Initialization
        for i = 1, MAX_SETS do
          ComboBoxAddMenuItem ("TwisterSetMenu",L""..i)
        end
    
        --Configuration of the relevant ComboBox Option
        --ButtonSetTextColor("TwisterSetMenuSelectedButton", Button.ButtonState.NORMAL, DefaultColor.RED.r, DefaultColor.RED.g, DefaultColor.RED.b)
        ButtonSetText ("TwisterSetMenuSelectedButton", L"1");
     end
end

function TwisterSet.OnMouseover()
if (deb) then DEBUG(L"OnMouseoverWorldMapBtn") end
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil )
	Tooltips.SetTooltipColor( 1, 1, 0, 90, 255 )
	Tooltips.SetTooltipText( 1, 1, L"Twister Set" )
	Tooltips.SetTooltipText( 3, 1, L"Change quickly your Auras" )
	Tooltips.Finalize()
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_LEFT)
end


function TwisterSet.OnLClick()

end



function TwisterSet.OnRClick()
--Trigger Twister to save the Set
Twister.SaveSet(tostring(TwisterSet.currentSet)); 
ButtonSetTextColor("TwisterSetMenuSelectedButton", Button.ButtonState.NORMAL, DefaultColor.RED.r, DefaultColor.RED.g, DefaultColor.RED.b)
ButtonSetText ("TwisterSetMenuSelectedButton", L""..TwisterSet.currentSet);

end



--Events triggered when the ComboBox value is changed
function TwisterSet.OnSetMenuSelectionChanged (currentSelection)
  TwisterSet.currentSet=currentSelection
  --Trigger Twister to load the Set  
  Twister.LoadSet(tostring(TwisterSet.currentSet)); 
  ButtonSetTextColor("TwisterSetMenuSelectedButton", Button.ButtonState.NORMAL, DefaultColor.WHITE.r, DefaultColor.WHITE.g, DefaultColor.WHITE.b)
  --Update Combobox relevant option Text  
  ButtonSetText ("TwisterSetMenuSelectedButton", L""..TwisterSet.currentSet);
 
end